/*
 ************************************************************************************
 * Copyright (C) 2017-2019 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.retail.deliveryconditions.processes;

import java.math.BigDecimal;

import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.common.plm.Product;

import com.openbravo.sharaf.integration.erpob.process.ImportProductStock;
import com.openbravo.sharaf.retail.customdevelopments.OBProductStock;

public class CheckStock extends BaseCheckStock {

  @Override
  protected BigDecimal getStock(Product product, Warehouse warehouse, String stockLocation) {
    OBProductStock stock = ImportProductStock.getProductStockByProductAndWarehouse(product,
        warehouse);
    if (stock == null) {
      return BigDecimal.ZERO;
    }

    return stock.getQuantityOnHand();
  }
}