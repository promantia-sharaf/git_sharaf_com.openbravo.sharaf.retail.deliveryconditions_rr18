/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.retail.deliveryconditions.term;

import java.util.ArrayList;
import java.util.List;

import org.openbravo.client.kernel.ComponentProvider.Qualifier;
import org.openbravo.mobile.core.model.HQLProperty;
import org.openbravo.retail.posterminal.term.Terminal;

@Qualifier(Terminal.terminalPropertyExtension)
public class CUSTDELTerminalProperties extends org.openbravo.retail.posterminal.term.TerminalProperties {
  @Override
  public List<HQLProperty> getHQLProperties(Object params) {
    
    ArrayList<HQLProperty> list = new ArrayList<>();

    list.add(new HQLProperty("pos.organization.custdelChecklocalstock", "custdelChecklocalstock"));
    return list;
  }
}
