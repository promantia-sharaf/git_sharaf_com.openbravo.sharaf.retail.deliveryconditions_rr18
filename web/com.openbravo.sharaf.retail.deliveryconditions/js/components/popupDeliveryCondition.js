/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone, _ */

enyo.kind({
  name: 'CUSTDEL.UI.DeliveryConditionsPopup',
  kind: 'OB.UI.ModalAction',
  i18nHeader: 'CUSTDEL_SelectDelivery',
  handlers: {
    onApplyChanges: 'applyChanges',
    onSetValue: 'setValue'
  },
  bodyContent: {
    kind: 'Scroller',
    maxHeight: '225px',
    style: 'background-color: #ffffff;',
    thumb: true,
    horizontal: 'hidden',
    components: [{
      name: 'attributes'
    }]
  },
  bodyButtons: {
    components: [{
      name: 'deliveryConditionApplyButton',
      kind: 'CUSTDEL.UI.DeliveryConditionsApply'
    }, {
      name: 'deliveryConditionCancelButton',
      kind: 'OB.UI.CancelDialogButton'
    }]
  },
  executeOnShow: function () {
    this.waterfall('onLoadValue');
    if (this.args || (this.args && this.args.mandatory === true)) {
      this.autoDismiss = false;
      this.closeOnEscKey = false;
      this.$.headerCloseButton.hide();
      this.$.bodyButtons.$.deliveryConditionCancelButton.hide();
    } else {
      this.autoDismiss = true;
      this.closeOnEscKey = true;
      this.$.headerCloseButton.show();
      this.$.bodyButtons.$.deliveryConditionCancelButton.show();
    }
  },
  applyChanges: function (inSender, inEvent) {
    var selectedDelivery = this.$.bodyContent.$.attributes.$.line_deliveryConditionsBox.$.newAttribute.$.deliveryConditionsBox.$.renderCombo.getValue();
    var selectedDeliveryLoc = this.$.bodyContent.$.attributes.$.line_deliveryLocationsBox.$.newAttribute.$.deliveryLocationsBox.$.renderCombo.getValue();
    OB.MobileApp.model.set('selectedDel', selectedDelivery);
    OB.MobileApp.model.set('selectedLoc', selectedDeliveryLoc);
    this.waterfall('onApplyChange', inEvent);
    return inEvent;

  },
  setValue: function (inSender, inEvent) {

    var i, j;
    for (j in inEvent.data) {
      if (inEvent.data.hasOwnProperty(j)) {
        for (i = 0; i < this.args.selectedModels.length; i++) {
          this.args.selectedModels[i].set(j, inEvent.data[j]);
        }
      }
    }
    return true;
  },
  initComponents: function () {
    this.inherited(arguments);
    this.attributeContainer = this.$.bodyContent.$.attributes;
    enyo.forEach(this.newAttributes, function (natt) {
      this.$.bodyContent.$.attributes.createComponent({
        kind: 'OB.UI.PropertyEditLine',
        name: 'line_' + natt.name,
        newAttribute: natt
      });
    }, this);
  }
});

enyo.kind({
  name: 'CUSTDEL.UI.DeliveryConditionsPopupImpl',
  kind: 'CUSTDEL.UI.DeliveryConditionsPopup',
  newAttributes: [{
    kind: 'CUSTDEL.UI.DeliveryConditionsBox',
    name: 'deliveryConditionsBox',
    i18nLabel: 'CUSTDEL_DeliveryCondition'
  }, {
    kind: 'CUSTDEL.UI.DeliveryLocationsBox',
    name: 'deliveryLocationsBox',
    i18nLabel: 'CUSTDEL_DeliveryLocation'
  }, {
    kind: 'CUSTDEL.UI.ReceiptDeliveryDate',
    name: 'receiptDeliveryDate',
    i18nLabel: 'CUSTDEL_DeliveryDate'
  }, {
    kind: 'CUSTDEL.UI.ReceiptDeliveryTime',
    name: 'receiptDeliveryTime',
    i18nLabel: 'CUSTDEL_DeliveryTime'
  }]
});

enyo.kind({
  name: 'CUSTDEL.UI.DeliveryConditionsBox',
  kind: 'OB.UI.renderComboProperty',
  events: {
    onSetValue: ''
  },
  handlers: {
    onchange: 'selectChanged'
  },
  modelProperty: 'cUSTDELDeliveryCondition',
  retrievedPropertyForValue: 'searchKey',
  retrievedPropertyForText: 'name',
  init: function (model) {
    this.model = model;
    this.collection = new Backbone.Collection();
    this.$.renderCombo.setCollection(this.collection);
    var i = 0;
    for (i; i < OB.MobileApp.model.get('sharafDeliveryConditions').length; i++) {
      model = new Backbone.Model(OB.MobileApp.model.get('sharafDeliveryConditions')[i]);
      this.collection.add(model);
    }
  },
  loadValue: function (inSender, inEvent) {
    this.setDeliveryConditionsCollection(inSender, inEvent);
    if (inSender.args.selectedModels.length >= 1) {
      this.$.renderCombo.setSelected(0);
      if (inSender.args.selectedModels[0].get(this.modelProperty)) {
        var i = 0;
        for (i; i < this.collection.length; i++) {
          if (this.collection.models[i].get('searchKey') === inSender.args.selectedModels[0].get(this.modelProperty)) {
            this.$.renderCombo.setSelected(i);
            break;
          }
        }
      }
    } else {
      this.$.renderCombo.setSelected(0);
    }
  },
  selectChanged: function (inSender, inEvent) {
    var id = inSender.getValue();
    var me = this.owner.owner.owner.owner.owner.attributeContainer.$.line_deliveryLocationsBox.$.newAttribute.$.deliveryLocationsBox;
    this.setNewDeliveryLocationsCollection(me, id);
    me.$.renderCombo.setSelected(0);
    if (me.collection.length > 0) {
      this.owner.owner.owner.owner.owner.attributeContainer.$.line_deliveryLocationsBox.setShowing(true);
      var i = 0;
      for (i; i < me.collection.length; i++) {
        if (me.collection.models[i].get('warehousename')) {
          me.$.renderCombo.setSelected(i);
          break;
        }
      }
    } else {
      this.owner.owner.owner.owner.owner.attributeContainer.$.line_deliveryLocationsBox.setShowing(false);
    }
  },

  setNewDeliveryLocationsCollection: function (me, id) {
    var wdDelObj, sdDelObj, imDelObj, hDelObj, pDelObj, cdDelObj, ddDelObj, ccDelObj, onDelObj;
    var orgWarehouseLocationObj = JSON.parse(localStorage.getItem('orgWarehouseLocation')).WarehouseLocation;

    if (id === 'WD') {
      wdDelObj = orgWarehouseLocationObj["WD"];
    }
    if (id === 'SD') {
      sdDelObj = orgWarehouseLocationObj["SD"];
    }
    if (id === 'IM') {
      imDelObj = orgWarehouseLocationObj["IM"];
    }
    if (id === 'H') {
      hDelObj = orgWarehouseLocationObj["H"];
    }
    if (id === 'P') {
      pDelObj = orgWarehouseLocationObj["P"];
    }
    if (id === 'DD') {
      cdDelObj = orgWarehouseLocationObj["DD"];
    }
    if (id === 'OD') {
      ddDelObj = orgWarehouseLocationObj["OD"];
    }
    if (id === 'C') {
      ccDelObj = orgWarehouseLocationObj["C"];
    }
    if (id === 'ON') {
      onDelObj = orgWarehouseLocationObj["ON"];
    }

    me.$.renderCombo.collection.reset(null);
    me.collection = new Backbone.Collection();
    me.$.renderCombo.setCollection(me.collection);
    if (!OB.UTIL.isNullOrUndefined(id)) {
      if (id === 'WD') {
        if (!OB.UTIL.isNullOrUndefined(wdDelObj)) {
          var wdLoc = wdDelObj.split(",");
          for (var i = 0; i < wdLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = wdLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                me.collection.add(model);
              }
            }
          }
        }
      } else if (id === 'SD') {
        if (!OB.UTIL.isNullOrUndefined(sdDelObj)) {
          var sdLoc = sdDelObj.split(",");
          for (var i = 0; i < sdLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = sdLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                me.collection.add(model);
              }
            }
          }
        }
      } else if (id === 'IM') {
        if (!OB.UTIL.isNullOrUndefined(imDelObj)) {
          var imLoc = imDelObj.split(",");
          for (var i = 0; i < imLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = imLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                me.collection.add(model);
              }
            }
          }
        }
      } else if (id === 'H') {
        if (!OB.UTIL.isNullOrUndefined(hDelObj)) {
          var hLoc = hDelObj.split(",");
          for (var i = 0; i < hLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = hLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                me.collection.add(model);
              }
            }
          }
        }
      } else if (id === 'P') {
        if (!OB.UTIL.isNullOrUndefined(pDelObj)) {
          var pLoc = pDelObj.split(",");
          for (var i = 0; i < pLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = pLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                me.collection.add(model);
              }
            }
          }
        }
      } else if (id === 'DD') {
        if (!OB.UTIL.isNullOrUndefined(cdDelObj)) {
          var cdLoc = cdDelObj.split(",");
          for (var i = 0; i < cdLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = cdLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                me.collection.add(model);
              }
            }
          }
        }
      } else if (id === 'OD') {
        if (!OB.UTIL.isNullOrUndefined(ddDelObj)) {
          var ddLoc = ddDelObj.split(",");
          for (var i = 0; i < ddLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = ddLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                me.collection.add(model);
              }
            }
          }
        }
      } else if (id === 'C') {
        if (!OB.UTIL.isNullOrUndefined(ccDelObj)) {
          var ccLoc = ccDelObj.split(",");
          for (var i = 0; i < ccLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = ccLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                me.collection.add(model);
              }
            }
          }
        }
      } else if (id === 'ON') {
        if (!OB.UTIL.isNullOrUndefined(onDelObj)) {
          var onLoc = onDelObj.split(",");
          for (var i = 0; i < onLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = onLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                me.collection.add(model);
              }
            }
          }
        }
      }
    }
  },
  applyChange: function (inSender, inEvent) {
    var selected = this.collection.at(this.$.renderCombo.getSelected());
    if (selected) {
      inEvent.data[this.modelProperty] = selected.get(this.retrievedPropertyForValue);
      return inEvent.data;
    }
  },
  setDeliveryConditionsCollection: function (inSender, inEvent) {
    // Set available Delivery Types based on items definition and document types
    var wdLine = false,
        spLine = false,
        imLine = false,
        hLine = false,
        pLine = false,
        cdLine = false,
        ddLine = false,
        ccLine = false,
        onLine = false,
        otherTypeLine = false,
        productDelivery = false,
        overrideRetDelivery = false,
        prodDeliveryName = "",
        delivery = "",
        verifiedReturn = "";

    _.each(inSender.args.selectedModels, function (line) {
      if (line.get('cUSTDELDeliveryCondition') === 'WD') {
        wdLine = true;
      } else if (line.get('cUSTDELDeliveryCondition') === 'SD') {
        spLine = true;
      } else if (line.get('cUSTDELDeliveryCondition') === 'IM') {
        imLine = true;
      } else if (line.get('cUSTDELDeliveryCondition') === 'H') {
        hLine = true;
      } else if (line.get('cUSTDELDeliveryCondition') === 'P' && OB.MobileApp.model.receipt.get('custsdtDocumenttypeSearchKey') === 'BP') {
        pLine = true;
      } else if (line.get('cUSTDELDeliveryCondition') === 'DD') {
        cdLine = true;
      } else if (line.get('cUSTDELDeliveryCondition') === 'OD') {
        ddLine = true;
      } else if (line.get('isVerifiedReturn')) {
        if (line.get('originalDeliveryCondition') === 'WD') {
          wdLine = true;
        } else if (line.get('originalDeliveryCondition') === 'SD') {
          spLine = true;
        } else if (line.get('originalDeliveryCondition') === 'IM' || line.get('originalDeliveryCondition') === 'ON' || line.get('originalDeliveryCondition') === 'P') {
          imLine = true;
        } else if (line.get('originalDeliveryCondition') === 'H') {
          hLine = true;
        } else if (line.get('originalDeliveryCondition') === 'P') {
          pLine = true;
        } else if (line.get('originalDeliveryCondition') === 'DD') {
          cdLine = true;
        } else if (line.get('originalDeliveryCondition') === 'OD') {
          ddLine = true;
        }
      } else {
        // if any delivery type is selected from the dropdown
        otherTypeLine = true;
        if (line.get('newDeliveryCondition') === 'WD') {
          wdLine = true;
        } else if (line.get('newDeliveryCondition') === 'SD') {
          spLine = true;
        } else if (line.get('newDeliveryCondition') === 'IM' || line.get('newDeliveryCondition') === 'ON' || line.get('newDeliveryCondition') === 'P') {
          imLine = true;
        } else if (line.get('newDeliveryCondition') === 'H') {
          hLine = true;
        } else if (line.get('newDeliveryCondition') === 'P') {
          pLine = true;
        } else if (line.get('newDeliveryCondition') === 'DD') {
          cdLine = true;
        } else if (line.get('newDeliveryCondition') === 'OD') {
          ddLine = true;
        } else if (line.get('originalDeliveryCondition') === 'C') {
          // Quantity increase or decrease in Bulk Sales
          if (line.get('product').get('cUSTDELDeliveryCondition') === 'WD') {
            wdLine = true;
          } else if (line.get('product').get('cUSTDELDeliveryCondition') === 'SD') {
            spLine = true;
          } else if (line.get('product').get('cUSTDELDeliveryCondition') === 'IM' || line.get('product').get('cUSTDELDeliveryCondition') === 'ON' || line.get('product').get('cUSTDELDeliveryCondition') === 'P') {
            imLine = true;
          } else if (line.get('product').get('cUSTDELDeliveryCondition') === 'H') {
            hLine = true;
          } else if (line.get('product').get('cUSTDELDeliveryCondition') === 'P') {
            pLine = true;
          } else if (line.get('product').get('cUSTDELDeliveryCondition') === 'DD') {
            cdLine = true;
          } else if (line.get('product').get('cUSTDELDeliveryCondition') === 'OD') {
            ddLine = true;
          }
        }
      }
      if (line.get('cUSTDELDeliveryCondition') === line.get('product').get('cUSTDELDeliveryCondition')) {
        productDelivery = true;
      } else if (otherTypeLine && line.get('originalDeliveryCondition') === line.get('product').get('cUSTDELDeliveryCondition')) {
        productDelivery = true;
      } else if (line.get('cUSTDELDeliveryCondition') === 'C' && line.get('originalDeliveryCondition') === 'C' && (wdLine || spLine || imLine || hLine || pLine || cdLine || ddLine || onLine)) {
        productDelivery = true;
      }

      if (line.get('cUSTDELDeliveryCondition') === 'C' && line.get('originalDeliveryCondition') === 'C' && !(wdLine && spLine && imLine && hLine && pLine && cdLine && ddLine && onLine)) {
        ccLine = true;
      }

      prodDeliveryName = line.get('product').get('cUSTDELDeliveryCondition');
      delivery = line.get('originalDeliveryCondition');
      verifiedReturn = line.get('isVerifiedReturn');

      if (!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.receipt.get('generatedFromQuotation')) && OB.MobileApp.model.receipt.get('generatedFromQuotation')) {
        delivery = line.get('product').get('cUSTDELDeliveryCondition');
        if (!OB.MobileApp.model.receipt.get('custsdtDocumenttypeSearchKey') === 'BP') {
          productDelivery = true;
        }
        if (line.get('cUSTDELDeliveryCondition') === 'ON' || line.get('cUSTDELDeliveryCondition') === 'P') {
          imLine = true;
        }
      }
    });

    this.$.renderCombo.collection.reset(null);
    this.collection = new Backbone.Collection();
    this.$.renderCombo.setCollection(this.collection);
    var i = 0;
    var delTypeArr = JSON.parse(localStorage.getItem('allowedDelivery'));
    if (delTypeArr.includes(delivery) && !OB.UTIL.isNullOrUndefined(verifiedReturn) && verifiedReturn) {
      productDelivery = true;
    }

    if (!productDelivery) {
      for (var p = 0; p < OB.MobileApp.model.get('sharafDeliveryConditions').length; p++) {
        for (var q = 0; q < delTypeArr.length; q++) {
          if (OB.MobileApp.model.get('sharafDeliveryConditions')[p].searchKey === delTypeArr[q]) {
            this.model = new Backbone.Model(OB.MobileApp.model.get('sharafDeliveryConditions')[p]);
            this.collection.add(this.model);
          }
        }
      }
    } else {
      var isCustomerCare = false,
          isOnlineDelivery = false,
          isSPickup = false,
          isOnlineCC = false,
          isCCSPickup = false,
          isOnlineSPickup = false,
          isOnlineCCSPickup = false;
      if (delTypeArr.includes('C')) {
        isCustomerCare = true;
      }
      if (delTypeArr.includes('ON')) {
        isOnlineDelivery = true;
      }
      if (delTypeArr.includes('P')) {
        isSPickup = true;
      }
      if (delTypeArr.includes('C') && delTypeArr.includes('ON')) {
        isOnlineCC = true;
      }
      if (delTypeArr.includes('C') && delTypeArr.includes('P')) {
        isCCSPickup = true;
      }
      if (delTypeArr.includes('ON') && delTypeArr.includes('P')) {
        isOnlineSPickup = true;
      }
      if (delTypeArr.includes('C') && delTypeArr.includes('ON') && delTypeArr.includes('P')) {
        isOnlineCCSPickup = true;
      }
      for (i; i < OB.MobileApp.model.get('sharafDeliveryConditions').length; i++) {
        if (hLine) {
          if (isCustomerCare) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'H' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'C') {
              continue;
            }
          } else if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'H') {
            continue;
          }
        } else if (pLine) {
          if (isCustomerCare) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'P' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'C') {
              continue;
            }
          } else if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'P') {
            continue;
          }
        } else if (wdLine) {
          if (isCustomerCare) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'WD' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'C') {
              continue;
            }
          } else if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'WD') {
            continue;
          }
        } else if (spLine) {
          if (isCustomerCare) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'SD' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'C') {
              continue;
            }
          } else if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'SD') {
            continue;
          }
        } else if (imLine) {
          if (isOnlineCCSPickup) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'IM' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'C' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'ON' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'P') {
              continue;
            }
          } else if (isOnlineCC) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'IM' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'C' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'ON') {
              continue;
            }
          } else if (isOnlineSPickup) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'IM' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'ON' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'P') {
              continue;
            }
          } else if (isCCSPickup) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'IM' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'C' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'P') {
              continue;
            }
          } else if (isCustomerCare) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'IM' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'C') {
              continue;
            }
          } else if (isOnlineDelivery) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'IM' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'ON') {
              continue;
            }
          } else if (isSPickup) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'IM' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'P') {
              continue;
            }
          } else if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'IM') {
            continue;
          }

        } else if (cdLine) {
          if (isCustomerCare) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'DD' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'C') {
              continue;
            }
          } else if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'DD') {
            continue;
          }
        } else if (ddLine) {
          if (isCustomerCare) {
            if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'OD' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'C') {
              continue;
            }
          } else if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'OD') {
            continue;
          }
        } else if (ccLine) {
          if (OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== 'C' && OB.MobileApp.model.get('sharafDeliveryConditions')[i].searchKey !== prodDeliveryName) {
            continue;
          }
        }
        this.model = new Backbone.Model(OB.MobileApp.model.get('sharafDeliveryConditions')[i]);
        this.collection.add(this.model);
      }

    }
  }
});

enyo.kind({
  name: 'CUSTDEL.UI.DeliveryLocationsBox',
  kind: 'OB.UI.renderComboProperty',
  events: {
    onSetValue: ''
  },
  modelProperty: 'cUSTDELDeliveryLocation',
  retrievedPropertyForValue: 'warehouseid',
  retrievedPropertyForText: 'warehousename',
  init: function (model) {
    this.model = model;
    this.collection = new Backbone.Collection();
    this.$.renderCombo.setCollection(this.collection);
    var i = 0;
    for (i; i < OB.MobileApp.model.get('warehouses').length; i++) {
      model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[i]);
      this.collection.add(model);
    }
  },
  loadValue: function (inSender, inEvent) {
    this.setDeliveryLocationsCollection(inSender, inEvent);
    if (inSender.args.selectedModels.length >= 1) {
      this.$.renderCombo.setSelected(0);
      if (OB.UTIL.isNullOrUndefined(inSender.args.selectedModels[0].get(this.modelProperty))) {
        inSender.args.selectedModels[0].set(this.modelProperty, inSender.args.selectedModels[0].id)
      }
      if (inSender.args.selectedModels[0].get(this.modelProperty) && this.collection.length > 0) {
        this.owner.owner.owner.owner.owner.attributeContainer.$.line_deliveryLocationsBox.setShowing(true);
        var i = 0;
        for (i; i < this.collection.length; i++) {
          if (inSender.args.selectedModels[0].get(this.modelProperty).length > 32) {
            if (this.collection.models[i].get('warehouseid') === inSender.args.selectedModels[0].get(this.modelProperty).substring(10, 42)) {
              this.$.renderCombo.setSelected(i);
              break;
            }
          } else {
            if (this.collection.models[i].get('warehouseid') === inSender.args.selectedModels[0].get(this.modelProperty)) {
              this.$.renderCombo.setSelected(i);
              break;
            }
          }
        }
      } else {
        this.owner.owner.owner.owner.owner.attributeContainer.$.line_deliveryLocationsBox.setShowing(false);
      }
    } else {
      this.$.renderCombo.setSelected(0);
    }
  },
  applyChange: function (inSender, inEvent) {
    var selected = this.collection.at(this.$.renderCombo.getSelected());
    if (selected) {
      inEvent.data[this.modelProperty] = selected.get(this.retrievedPropertyForValue);
      return inEvent.data;
    } else if (OB.MobileApp.model.receipt.get('generatedFromQuotation')) {
      inSender.args.selectedModels[0].attributes.warehouse = OB.MobileApp.model.get('warehouses')[0].warehouseid;
      inSender.args.selectedModels[0].attributes.warehousename = OB.MobileApp.model.get('warehouses')[0].warehousename;
    }
  },
  setDeliveryLocationsCollection: function (inSender, inEvent) {

    var wdDelObj, sdDelObj, imDelObj, hDelObj, pDelObj, cdDelObj, ddDelObj, ccDelObj, onDelObj;
    var orgWarehouseLocationObj = JSON.parse(localStorage.getItem('orgWarehouseLocation')).WarehouseLocation;

    _.each(inSender.args.selectedModels, function (line) {
      if (line.get('cUSTDELDeliveryCondition') === 'WD') {
        wdDelObj = orgWarehouseLocationObj["WD"];
      }
      if (line.get('cUSTDELDeliveryCondition') === 'SD') {
        sdDelObj = orgWarehouseLocationObj["SD"];
      }
      if (line.get('cUSTDELDeliveryCondition') === 'IM') {
        imDelObj = orgWarehouseLocationObj["IM"];
      }
      if (line.get('cUSTDELDeliveryCondition') === 'H') {
        hDelObj = orgWarehouseLocationObj["H"];
      }
      if (line.get('cUSTDELDeliveryCondition') === 'P') {
        pDelObj = orgWarehouseLocationObj["P"];
      }
      if (line.get('cUSTDELDeliveryCondition') === 'DD') {
        cdDelObj = orgWarehouseLocationObj["DD"];
      }
      if (line.get('cUSTDELDeliveryCondition') === 'OD') {
        ddDelObj = orgWarehouseLocationObj["OD"];
      }
      if (line.get('cUSTDELDeliveryCondition') === 'C') {
        ccDelObj = orgWarehouseLocationObj["C"];
      }
      if (line.get('cUSTDELDeliveryCondition') === 'ON') {
        onDelObj = orgWarehouseLocationObj["ON"];
      }
    });

    this.$.renderCombo.collection.reset(null);
    this.collection = new Backbone.Collection();
    this.$.renderCombo.setCollection(this.collection);
    if (!OB.UTIL.isNullOrUndefined(inSender.args.selectedModels[0].attributes.cUSTDELDeliveryCondition)) {
      if (inSender.args.selectedModels[0].attributes.cUSTDELDeliveryCondition === 'WD') {
        if (!OB.UTIL.isNullOrUndefined(wdDelObj)) {
          var wdLoc = wdDelObj.split(",");
          for (var i = 0; i < wdLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = wdLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (!OB.UTIL.isNullOrUndefined(loc)) {
                if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                  model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                  this.collection.add(model);
                }
              }
            }
          }
        }
      } else if (inSender.args.selectedModels[0].attributes.cUSTDELDeliveryCondition === 'SD') {
        if (!OB.UTIL.isNullOrUndefined(sdDelObj)) {
          var sdLoc = sdDelObj.split(",");
          for (var i = 0; i < sdLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = sdLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                this.collection.add(model);
              }
            }
          }
        }
      } else if (inSender.args.selectedModels[0].attributes.cUSTDELDeliveryCondition === 'IM') {
        if (!OB.UTIL.isNullOrUndefined(imDelObj)) {
          var imLoc = imDelObj.split(",");
          for (var i = 0; i < imLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = imLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                this.collection.add(model);
              }
            }
          }
        }
      } else if (inSender.args.selectedModels[0].attributes.cUSTDELDeliveryCondition === 'H') {
        if (!OB.UTIL.isNullOrUndefined(hDelObj)) {
          var hLoc = hDelObj.split(",");
          for (var i = 0; i < hLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = hLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                this.collection.add(model);
              }
            }
          }
        }
      } else if (inSender.args.selectedModels[0].attributes.cUSTDELDeliveryCondition === 'P') {
        if (!OB.UTIL.isNullOrUndefined(pDelObj)) {
          var pLoc = pDelObj.split(",");
          for (var i = 0; i < pLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = pLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                this.collection.add(model);
              }
            }
          }
        }
      } else if (inSender.args.selectedModels[0].attributes.cUSTDELDeliveryCondition === 'DD') {
        if (!OB.UTIL.isNullOrUndefined(cdDelObj)) {
          var cdLoc = cdDelObj.split(",");
          for (var i = 0; i < cdLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = cdLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                this.collection.add(model);
              }
            }
          }
        }
      } else if (inSender.args.selectedModels[0].attributes.cUSTDELDeliveryCondition === 'OD') {
        if (!OB.UTIL.isNullOrUndefined(ddDelObj)) {
          var ddLoc = ddDelObj.split(",");
          for (var i = 0; i < ddLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = ddLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                this.collection.add(model);
              }
            }
          }
        }
      } else if (inSender.args.selectedModels[0].attributes.cUSTDELDeliveryCondition === 'C') {
        if (!OB.UTIL.isNullOrUndefined(ccDelObj)) {
          var ccLoc = ccDelObj.split(",");
          for (var i = 0; i < ccLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = ccLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                this.collection.add(model);
              }
            }
          }
        }
      } else if (inSender.args.selectedModels[0].attributes.cUSTDELDeliveryCondition === 'ON') {
        if (!OB.UTIL.isNullOrUndefined(onDelObj)) {
          var onLoc = onDelObj.split(",");
          for (var i = 0; i < onLoc.length; i++) {
            for (var j = 0; j < OB.MobileApp.model.get('warehouses').length; j++) {
              var split = onLoc[i].replace("[", "");
              var loc = split.replace("]", "");
              if (loc.trim() === OB.MobileApp.model.get('warehouses')[j].warehousename) {
                model = new Backbone.Model(OB.MobileApp.model.get('warehouses')[j]);
                this.collection.add(model);
              }
            }
          }
        }
      }
    }
  }
});

enyo.kind({
  name: 'CUSTDEL.UI.ReceiptDeliveryDate',
  kind: 'onyx.DatePicker',
  modelProperty: 'cUSTDELDeliveryTime',
  events: {
    onSetValue: ''
  },
  handlers: {
    onLoadValue: 'loadValue',
    onApplyChange: 'applyChange'
  },
  loadValue: function (inSender, inEvent) {
    this.setLocale(OB.MobileApp.model.get('terminal').language_string);
    if (inSender.args.selectedModels.length === 1) {
      if (inSender.args.selectedModels[0].get(this.modelProperty)) {
        this.setValue(new Date(inSender.args.selectedModels[0].get(this.modelProperty)));
      } else {
        this.setValue(new Date());
      }
    } else {
      this.setValue(new Date());
    }
  },
  applyChange: function (inSender, inEvent) {
    inEvent.data[this.modelProperty] = this.getValue();
    return inEvent.data;
  }
});

enyo.kind({
  name: 'CUSTDEL.UI.ReceiptDeliveryTime',
  kind: 'onyx.TimePicker',
  modelProperty: 'cUSTDELDeliveryTime',
  events: {
    onSetValue: ''
  },
  handlers: {
    onLoadValue: 'loadValue',
    onApplyChange: 'applyChange'
  },
  loadValue: function (inSender, inEvent) {
    this.setLocale(OB.MobileApp.model.get('terminal').language_string);
    if (inSender.args.selectedModels.length === 1) {
      if (inSender.args.selectedModels[0].get(this.modelProperty)) {
        this.setValue(new Date(inSender.args.selectedModels[0].get(this.modelProperty)));
      } else {
        this.setValue(new Date());
      }
    } else {
      this.setValue(new Date());
    }
  },
  applyChange: function (inSender, inEvent) {
    var value = this.getValue(),
        deliveryTime = inEvent.data.cUSTDELDeliveryTime;
    if (value) {
      value.setSeconds(0);
    }
    inEvent.data[this.modelProperty] = new Date(deliveryTime.getFullYear(), deliveryTime.getMonth(), deliveryTime.getDate(), this.value.getHours(), this.value.getMinutes(), this.value.getSeconds());
    return inEvent.data;
  }
});

enyo.kind({
  name: 'CUSTDEL.UI.DeliveryConditionsApply',
  kind: 'OB.UI.ModalDialogButton',
  events: {
    onHideThisPopup: '',
    onApplyChanges: '',
    onSetValue: ''
  },
  tap: function () {
    var me = this,
        inEvent = {
        data: {}
        },
        callbackSuccess = function () {
        me.doSetValue(inEvent);
        me.doHideThisPopup();
        me.model.get('order').save();
        me.model.get('orderList').saveCurrent();
        if (me.owner.owner.args.callback instanceof Function) {
          me.owner.owner.args.callback();
        }
        };
    this.doApplyChanges(inEvent);
    OB.UTIL.showLoading(true);

    if (!OB.UTIL.isNullOrUndefined(inEvent.data.cUSTDELDeliveryLocation)) {
      for (var i = 0; i < OB.MobileApp.model.get('warehouses').length; i++) {
        if (OB.MobileApp.model.get('warehouses')[i].warehouseid === inEvent.data.cUSTDELDeliveryLocation) {
          me.owner.owner.args.selectedModels[0].set('cUSTDELDeliveryLocation', OB.MobileApp.model.get('warehouses')[i].warehousename);
        }
      }
      _.each(me.owner.owner.args.selectedModels, function (line) {
        if (!OB.UTIL.isNullOrUndefined(line.attributes.shaquoOriginalDocumentNo)) {
          line.attributes.warehouse = inEvent.data.cUSTDELDeliveryLocation;
          line.attributes.warehousename = me.owner.owner.args.selectedModels[0].get('cUSTDELDeliveryLocation');
        } else {
          line.attributes.warehouse.id = inEvent.data.cUSTDELDeliveryLocation;
          line.attributes.warehouse.warehousename = me.owner.owner.args.selectedModels[0].get('cUSTDELDeliveryLocation');
        }
      });
    } else {
      _.each(me.owner.owner.args.selectedModels, function (line) {
        line.attributes.warehouse.id = OB.MobileApp.model.get('warehouses')[0].warehouseid;
        line.attributes.warehouse.warehousename = OB.MobileApp.model.get('warehouses')[0].warehousename;
      });
    }

    var deliveryTypeSK = OB.MobileApp.model.get('selectedDel'),
        stockLocArr = OB.MobileApp.model.get('sharafStockLocation'),
        sharafDelivery = OB.MobileApp.model.get('sharafDeliveryConditions'),
        deliveryType = "",
        stockCheckLocation = "",
        warehouse = "";
    for (var i = 0; i < sharafDelivery.length; i++) {
      if (deliveryTypeSK === sharafDelivery[i].searchKey) {
        deliveryType = sharafDelivery[i].searchKey;
      }
    }
    for (var i = 0; i < stockLocArr.length; i++) {
      if (stockLocArr[i].organization === OB.MobileApp.model.attributes.terminal.organization$_identifier && deliveryType === stockLocArr[i].deliveryType) {
        stockCheckLocation = stockLocArr[i].stockLocation;
      }
    }

    if (_.isEmpty(stockCheckLocation)) {
      for (var i = 0; i < stockLocArr.length; i++) {
        if (deliveryType === stockLocArr[i].deliveryType && stockLocArr[i].organization === '*') {
          stockCheckLocation = stockLocArr[i].stockLocation;
          break;
        }
      }
    }

    getProductTotalQtyInReceiptForDelivery = function (currentLine, receipt, deliveryType) {
      var receiptQty = _.reduce(receipt.get('lines').models, function (memo, line, index) {
        if (line.get('id') !== currentLine.get('id') && line.get('product').get('id') === currentLine.get('product').get('id') && line.get('warehouse').id === currentLine.get('warehouse').id && line.get('cUSTDELDeliveryCondition') === deliveryType) {
          if (OB.UTIL.isNullOrUndefined(line.get('custdisOrderline')) && line.get('product').get('productType') !== 'S') {
            return OB.DEC.add(memo, line.get('qty'));
          } else {
            return OB.DEC.add(memo, OB.DEC.Zero);
          }
        } else {
          return OB.DEC.add(memo, OB.DEC.Zero);
        }
      }, OB.DEC.Zero);
      return OB.DEC.add(receiptQty, currentLine.get('qty'));
    }

    getWarehouseDeliveryItemsWarehouse = _.find(OB.MobileApp.model.get('warehouses'), function (warehouse) {
      if (!OB.UTIL.isNullOrUndefined(warehouse.cUSTDELDeliveryType)) {
        return warehouse.cUSTDELDeliveryType === deliveryType;
      }
    });

    if (!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.get('selectedLoc')) && OB.MobileApp.model.get('selectedLoc').length > 0) {
      warehouse = OB.MobileApp.model.get('selectedLoc');
    } else if (!OB.UTIL.isNullOrUndefined(getWarehouseDeliveryItemsWarehouse)) {
      warehouse = getWarehouseDeliveryItemsWarehouse.warehouseid;
    }

    var productId = me.owner.owner.args.selectedModels[0].get('product').get('id'),
        qty = getProductTotalQtyInReceiptForDelivery(me.owner.owner.args.selectedModels[0], OB.MobileApp.model.receipt, deliveryType);;
    if (!_.isEmpty(stockCheckLocation) && !OB.UTIL.isNullOrUndefined(warehouse) && warehouse.length > 0) {
      var process = new OB.DS.Request('com.openbravo.sharaf.retail.deliveryconditions.processes.CheckStockSAPService');
      process.exec({
        productId: productId,
        qty: qty,
        warehouse: warehouse,
        stockLocation: stockCheckLocation
      }, function (data) {
        if (data && data.exception) {
          OB.UTIL.showLoading(false);
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTDEL_Error'), [{
            label: OB.I18N.getLabel('OBMOBC_LblOk')
          }]);
          return false;
        } else {
          OB.UTIL.showLoading(false);
          callbackSuccess();
        }
      }, function () {
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTDEL_Error'), [{
          label: OB.I18N.getLabel('OBMOBC_LblOk')
        }]);
      });
    } else {
      OB.UTIL.showLoading(false);
      callbackSuccess();
    }

/* if (inEvent.data && (inEvent.data.cUSTDELDeliveryCondition === 'WD' || (OB.CUSTDEL.checkImmediateDeliveryItemsStock() && inEvent.data.cUSTDELDeliveryCondition === 'IM'))) {
      OB.UTIL.showLoading(true);
      var errors = [],
          checkStockInOB = OB.CUSTDEL.checkImmediateDeliveryItemsStock() && inEvent.data.cUSTDELDeliveryCondition === 'IM';
      var finalCheckReceiptStock = _.after(me.owner.owner.args.selectedModels.length, function () {
        if (errors.length > 0) {
          OB.UTIL.showLoading(false);
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTDEL_Error'), errors.join());
        } else {
          OB.UTIL.showLoading(false);
          callbackSuccess();
        }
      });
      _.each(me.owner.owner.args.selectedModels, function (line) {
        if (line.get('qty') > 0 && (!checkStockInOB || (checkStockInOB && OB.UTIL.isNullOrUndefined(line.get('custdisOrderline')) && line.get('product').get('productType') !== 'S'))) {
          OB.CUSTDEL.checkStock(line.get('product').get('id'), OB.CUSTDEL.getProductTotalQtyInReceiptForWDorIM(line, OB.MobileApp.model.receipt, inEvent.data.cUSTDELDeliveryCondition), checkStockInOB, function () {
            finalCheckReceiptStock();
          }, function () {
            errors.push(line.get('product').get(OB.Constants.IDENTIFIER));
            finalCheckReceiptStock();
          });
        } else {
          finalCheckReceiptStock();
        }
      });
    } else {*/
    //  callbackSuccess();
    // }
  },
  initComponents: function () {
    this.inherited(arguments);
    this.setContent(OB.I18N.getLabel('OBMOBC_LblApply'));
  },
  init: function (model) {
    this.model = model;
  }
});

OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
  kind: 'CUSTDEL.UI.DeliveryConditionsPopupImpl',
  name: 'CUSTDEL_UI_DeliveryConditionsPopup'
});