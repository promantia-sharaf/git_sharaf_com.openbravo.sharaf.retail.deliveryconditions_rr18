(function () {
  OB.UTIL.HookManager.registerHook('OBPOS_BeforeCustomerSave', function (args, callbacks) {
	  if (args.isNew){
		  if(args.customer.get('cityName') === ''){
			  OB.UTIL.showError('City field is mandatory and cannot be left empty');
			  args.windowComponent.waterfall('onDisableButton', {
				  disabled: false
			  });
			  args.cancellation = true;
			  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
		  }else{
			  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
		  }
	  }else{
		  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
	  }
  });
}());